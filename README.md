##### Creating an Order
```
curl --request POST \
  --url http://127.0.0.1:8000/api/orders \
  --header 'content-type: application/json' \
  --data '{
	"customerId": "customer",
	"deliveryDate": "2018-01-01",
	"deliveryWindowStart": 10,
	"deliveryWindowEnd": 18
}'
```

##### Fetching an Order by Date and Driver
```
curl --request GET \
  --url 'http://127.0.0.1:8000/api/orders?driverId=RANDOM_DRIVER_ID&deliveryDate=2018-01-01' \
  --header 'content-type: application/json'
```
