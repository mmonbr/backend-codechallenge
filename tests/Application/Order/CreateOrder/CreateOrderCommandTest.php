<?php

namespace Tests\Application\Order\CreateOrder;

use App\Application\Order\CreateOrder\CreateOrderCommand;
use Tests\TestCase;

class CreateOrderCommandTest extends TestCase
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * @var \DateTime
     */
    private $deliveryDate;

    /**
     * @var int
     */
    private $deliveryWindowStart;

    /**
     * @var int
     */
    private $deliveryWindowEnd;

    /**
     * @var CreateOrderCommand
     */
    private $command;

    protected function setUp(): void
    {
        $this->customerId = 'customerId';
        $this->deliveryDate = new \DateTime();
        $this->deliveryWindowStart = 10;
        $this->deliveryWindowEnd = 18;

        $this->command = new CreateOrderCommand(
            $this->customerId,
            $this->deliveryDate,
            $this->deliveryWindowStart,
            $this->deliveryWindowEnd
        );
    }

    public function testCustomerId(): void
    {
        $this->assertEquals($this->customerId, $this->command->customerId());
    }

    public function testDeliveryDate(): void
    {
        $this->assertEquals($this->deliveryDate, $this->command->deliveryDate());

    }

    public function testDeliveryWindowStart(): void
    {
        $this->assertEquals($this->deliveryWindowStart, $this->command->deliveryWindowStart());

    }

    public function testDeliveryWindowEnd(): void
    {
        $this->assertEquals($this->deliveryWindowEnd, $this->command->deliveryWindowEnd());

    }
}
