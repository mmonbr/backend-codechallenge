<?php

namespace Tests\Application\Order\CreateOrder;

use App\Application\Order\CreateOrder\CreateOrderCommand;
use App\Application\Order\CreateOrder\CreateOrderHandler;
use App\Domain\Customer\Address;
use App\Domain\Customer\Customer;
use App\Domain\Customer\CustomerRepository;
use App\Domain\Order\Order;
use App\Domain\Order\OrderFactory;
use App\Domain\Order\OrderRepository;
use Doctrine\Common\Collections\Collection;
use Illuminate\Contracts\Events\Dispatcher;
use Tests\TestCase;

\DG\BypassFinals::enable();

class CreateOrderHandlerTest extends TestCase
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var CreateOrderHandler
     */
    private $handler;

    protected function setUp(): void
    {
        $this->customerRepository = $this->createMock(CustomerRepository::class);
        $this->orderRepository = $this->createMock(OrderRepository::class);
        $this->orderFactory = $this->createMock(OrderFactory::class);
        $this->dispatcher = $this->createMock(Dispatcher::class);

        $this->handler = new CreateOrderHandler(
            $this->customerRepository,
            $this->orderRepository,
            $this->orderFactory,
            $this->dispatcher
        );
    }

    public function testHandle(): void
    {
        $customerId = 'customerId';
        $deliveryDate = new \DateTime();
        $deliveryWindowStart = 10;
        $deliveryWindowEnd = 18;

        $address = $this->createMock(Address::class);

        $adddresses = $this->createConfiguredMock(Collection::class, [
            'first' => $address
        ]);

        $customer = $this->createConfiguredMock(Customer::class, [
            'addresses' => $adddresses
        ]);

        $this->customerRepository
            ->expects($this->once())
            ->method('findOrFail')
            ->willReturn($customer);

        $order = $this->createMock(Order::class);

        $this->orderFactory
            ->expects($this->once())
            ->method('create')
            ->willReturn($order);

        $this->dispatcher
            ->expects($this->once())
            ->method('dispatch');

        $command = new CreateOrderCommand(
            $customerId,
            $deliveryDate,
            $deliveryWindowStart,
            $deliveryWindowEnd
        );

        $this->handler->handle($command);
    }
}
