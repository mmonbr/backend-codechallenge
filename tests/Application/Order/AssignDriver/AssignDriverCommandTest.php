<?php

namespace Tests\Application\Order\AssignDriver;

use App\Application\Order\AssignDriver\AssignDriverCommand;
use Tests\TestCase;

class AssignDriverCommandTest extends TestCase
{
    /**
     * @var string
     */
    private $orderId;

    /**
     * @var AssignDriverCommand
     */
    private $command;

    protected function setUp(): void
    {
        $this->orderId = 'orderId';

        $this->command = new AssignDriverCommand(
            $this->orderId
        );
    }

    public function testOrderId(): void
    {
        $this->assertEquals($this->orderId, $this->command->orderId());
    }
}
