<?php

namespace Tests\Application\Order\AssignDriver;

use App\Application\Order\AssignDriver\AssignDriverCommand;
use App\Application\Order\AssignDriver\AssignDriverHandler;
use App\Domain\Order\Order;
use App\Domain\Order\OrderRepository;
use Tests\TestCase;

\DG\BypassFinals::enable();

class AssignDriverHandlerTest extends TestCase
{
    /**
     * @var OrderRepository
     */
    private $repository;

    /**
     * @var AssignDriverHandler
     */
    private $handler;

    protected function setUp(): void
    {
        $this->repository = $this->createMock(OrderRepository::class);

        $this->handler = new AssignDriverHandler(
            $this->repository
        );
    }

    public function testHandle(): void
    {
        $orderId = 'orderId';
        $command = new AssignDriverCommand($orderId);

        $order = $this->createMock(Order::class);

        $this->repository->expects($this->once())->method('findOrFail')->with($orderId)->willReturn($order);

        $this->repository->expects($this->once())->method('save')->with($order);

        $this->handler->handle($command);
    }
}
