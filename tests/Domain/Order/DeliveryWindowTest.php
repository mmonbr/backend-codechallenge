<?php

namespace Tests\Domain\Order;

use App\Domain\Order\DeliveryWindow;
use Tests\TestCase;

class DeliveryWindowTest extends TestCase
{
    public function testCreate(): void
    {
        $deliveryWindow = new DeliveryWindow(10, 18);

        $this->assertEquals(10, $deliveryWindow->start());
        $this->assertEquals(18, $deliveryWindow->end());
    }

    /**
     * @dataProvider validationProvider
     */
    public function testValidation($start, $end, $expectedException): void
    {
        $this->expectException($expectedException);

        new DeliveryWindow($start, $end);
    }

    public function validationProvider(): array
    {
        return [
            [10, 8, \Exception::class],
            [10, 10, \Exception::class],
            [10, 9, \Exception::class],
        ];
    }
}
