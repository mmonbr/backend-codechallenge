<?php

namespace Tests\Domain\Order;

use App\Domain\Order\OrderFactory;
use Tests\TestCase;

class OrderFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $factory = new OrderFactory();

        $firstName = 'firstName';
        $lastName = 'lastName';
        $email = 'email';
        $phoneNumber = 'phoneNumber';
        $street = 'street';
        $city = 'city';
        $state = 'state';
        $country = 'country';
        $zipCode = 'zipCode';
        $deliveryDate = new \DateTime();
        $deliveryStart = 10;
        $deliveryEnd = 18;

        $order = $factory->create(
            $firstName,
            $lastName,
            $email,
            $phoneNumber,
            $street,
            $city,
            $state,
            $country,
            $zipCode,
            $deliveryDate,
            $deliveryStart,
            $deliveryEnd
        );

        $this->assertEquals($firstName, $order->firstName());
        $this->assertEquals($lastName, $order->lastName());
        $this->assertEquals($email, $order->email());
        $this->assertEquals($phoneNumber, $order->phoneNumber());
        $this->assertEquals($street, $order->address()->street());
        $this->assertEquals($city, $order->address()->city());
        $this->assertEquals($state, $order->address()->state());
        $this->assertEquals($country, $order->address()->country());
        $this->assertEquals($zipCode, $order->address()->zipCode());
        $this->assertEquals($deliveryDate, $order->deliveryDate());
        $this->assertEquals($deliveryStart, $order->deliveryWindow()->start());
        $this->assertEquals($deliveryEnd, $order->deliveryWindow()->end());
    }
}
