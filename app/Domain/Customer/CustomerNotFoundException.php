<?php

namespace App\Domain\Customer;

final class CustomerNotFoundException extends \Exception
{
}
