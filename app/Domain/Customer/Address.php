<?php

namespace App\Domain\Customer;

final class Address
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $street;
    /**
     * @var string
     */
    private $city;
    /**
     * @var string
     */
    private $state;
    /**
     * @var string
     */
    private $country;
    /**
     * @var string
     */
    private $zipCode;

    /**
     * @var Customer
     */
    private $customer;

    public function __construct(
        string $street,
        string $city,
        string $state,
        string $country,
        string $zipCode,
        Customer $customer
    ) {
        $this->street = $street;
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
        $this->zipCode = $zipCode;
        $this->customer = $customer;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function street(): string
    {
        return $this->street;
    }

    public function city(): string
    {
        return $this->city;
    }

    public function state(): string
    {
        return $this->state;
    }

    public function country(): string
    {
        return $this->country;
    }

    public function zipCode(): string
    {
        return $this->zipCode;
    }

    public function customer(): Customer
    {
        return $this->customer;
    }
}
