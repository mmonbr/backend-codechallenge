<?php

namespace App\Domain\Customer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

final class Customer
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var Collection
     */
    private $addresses;

    public function __construct(
        string $firstName,
        string $lastName,
        string $email,
        string $phoneNumber
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->phoneNumber = $phoneNumber;

        $this->addresses = new ArrayCollection();
    }

    public function id(): string
    {
        return $this->id;
    }

    public function firstName(): string
    {
        return $this->firstName;
    }

    public function lastName(): string
    {
        return $this->lastName;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function phoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function addresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(
        string $street,
        string $city,
        string $state,
        string $country,
        string $zipCode
    ): void {
        $this->addresses[] = new Address(
            $street,
            $city,
            $state,
            $country,
            $zipCode,
            $this
        );
    }
}
