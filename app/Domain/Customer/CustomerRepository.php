<?php

namespace App\Domain\Customer;

interface CustomerRepository
{
    public function findOrFail(string $id): Customer;
}
