<?php

namespace App\Domain\Order;

final class Order
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var Address
     */
    private $address;

    /**
     * @var \DateTime
     */
    private $deliveryDate;

    /**
     * @var DeliveryWindow
     */
    private $deliveryWindow;

    /**
     * @var string
     */
    private $driver;

    /**
     * @var \DateTime|null
     */
    private $deliveredAt;

    public function __construct(
        string $firstName,
        string $lastName,
        string $email,
        string $phoneNumber,
        \DateTime $deliveryDate,
        Address $address,
        DeliveryWindow $deliveryWindow
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->phoneNumber = $phoneNumber;
        $this->deliveryDate = $deliveryDate;
        $this->address = $address;
        $this->deliveryWindow = $deliveryWindow;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function firstName(): string
    {
        return $this->firstName;
    }

    public function lastName(): string
    {
        return $this->lastName;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function phoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function deliveryDate(): \DateTime
    {
        return $this->deliveryDate;
    }

    public function address(): Address
    {
        return $this->address;
    }

    public function deliveryWindow(): DeliveryWindow
    {
        return $this->deliveryWindow;
    }

    public function driver(): string
    {
        return $this->driver;
    }

    public function setDriver(string $driver): void
    {
        $this->driver = $driver;
    }

    public function deliveredAt(): ?\DateTime
    {
        return $this->deliveredAt;
    }

    public function deliver(): void
    {
        $this->deliveredAt = new \DateTime();
    }
}
