<?php

namespace App\Domain\Order;

final class OrderFactory
{
    public function create(
        string $firstName,
        string $lastName,
        string $email,
        string $phoneNumber,
        string $street,
        string $city,
        string $state,
        string $country,
        string $zipCode,
        \DateTime $deliveryDate,
        int $deliveryStart,
        int $deliveryEnd
    ): Order {
        $address = new Address(
            $street,
            $city,
            $state,
            $country,
            $zipCode
        );

        $deliveryWindow = new DeliveryWindow($deliveryStart, $deliveryEnd);

        return new Order(
            $firstName,
            $lastName,
            $email,
            $phoneNumber,
            $deliveryDate,
            $address,
            $deliveryWindow
        );
    }
}
