<?php

namespace App\Domain\Order;

final class DeliveryWindow
{
    const MINIMUM_WINDOW_HOURS = 1;
    const MAXIMUM_WINDOW_HOURS = 8;

    /**
     * @var int
     */
    private $start;

    /**
     * @var int
     */
    private $end;

    public function __construct(int $start, int $end)
    {
        $this->start = $start;
        $this->end = $end;

        $this->validate();
    }

    public function start(): int
    {
        return $this->start;
    }

    public function end(): int
    {
        return $this->end;
    }

    private function validate(): void
    {
        if ($this->start() > $this->end()) {
            throw new \Exception('Invalid delivery window. Start hour must be higher than end.');
        }

        if ($this->start() === $this->end()) {
            throw new \Exception('Invalid delivery window. Start date and end hour cannot be the same.');
        }

        $start = $this->start();
        $end = $this->end();

        $isInRange = (($end - $start) >= self::MINIMUM_WINDOW_HOURS && ($end - $start <= self::MAXIMUM_WINDOW_HOURS));

        if (false === $isInRange) {
            throw new \Exception(
                'Invalid delivery window. Maximum difference between start and end cannot be greater than 8 hours.'
            );
        }
    }
}
