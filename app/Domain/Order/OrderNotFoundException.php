<?php

namespace App\Domain\Order;

final class OrderNotFoundException extends \Exception
{
}
