<?php

namespace App\Domain\Order;

interface OrderRepository
{
    public function findOrFail(string $id): Order;

    /**
     * @return Order[]
     */
    public function findAll(?OrderFilter $filter = null): array;

    public function save(Order $order): void;
}
