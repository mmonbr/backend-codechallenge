<?php

namespace App\Domain\Order;

final class OrderFilter
{
    /**
     * @var \DateTime|null
     */
    private $deliveryDate;

    /***
     * @var string|null
     */
    private $driverId;

    public function deliveryDate(): ?\DateTime
    {
        return $this->deliveryDate;
    }

    public function setDeliveryDate(?\DateTime $date): void
    {
        $this->deliveryDate = $date;
    }

    public function driverId(): ?string
    {
        return $this->driverId;
    }

    public function setDriverId(?string $driverId): void
    {
        $this->driverId = $driverId;
    }
}
