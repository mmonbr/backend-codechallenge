<?php

namespace App\Application\Order\CreateOrder;

use App\Domain\Customer\CustomerRepository;
use App\Domain\Order\Address;
use App\Domain\Order\OrderCreated;
use App\Domain\Order\OrderFactory;
use App\Domain\Order\OrderRepository;
use Illuminate\Contracts\Events\Dispatcher;

final class CreateOrderHandler
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    public function __construct(
        CustomerRepository $customerRepository,
        OrderRepository $orderRepository,
        OrderFactory $orderFactory,
        Dispatcher $dispatcher
    ) {
        $this->customerRepository = $customerRepository;
        $this->orderRepository = $orderRepository;
        $this->orderFactory = $orderFactory;
        $this->dispatcher = $dispatcher;
    }

    public function handle(
        CreateOrderCommand $command
    ): void {
        $customerId = $command->customerId();
        $customer = $this->customerRepository->findOrFail($customerId);

        /** @var Address $address */
        $address = $customer->addresses()->first();

        $order = $this->orderFactory->create(
            $customer->firstName(),
            $customer->lastName(),
            $customer->email(),
            $customer->phoneNumber(),
            $address->street(),
            $address->city(),
            $address->state(),
            $address->country(),
            $address->zipCode(),
            $command->deliveryDate(),
            $command->deliveryWindowStart(),
            $command->deliveryWindowEnd()
        );

        $this->orderRepository->save($order);

        $orderId = $order->id();
        $event = new OrderCreated($orderId);

        $this->dispatcher->dispatch($event);
    }
}
