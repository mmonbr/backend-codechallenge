<?php

namespace App\Application\Order\CreateOrder;

final class CreateOrderCommand
{
    /**
     * @var string
     */
    private $clientId;

    /**
     * @var \DateTime
     */
    private $deliveryDate;

    /**
     * @var int
     */
    private $deliveryWindowStart;

    /**
     * @var int
     */
    private $deliveryWindowEnd;

    public function __construct(
        string $customerId,
        \DateTime $deliveryDate,
        int $deliveryWindowStart,
        int $deliveryWindowEnd
    ) {
        $this->clientId = $customerId;
        $this->deliveryDate = $deliveryDate;
        $this->deliveryWindowStart = $deliveryWindowStart;
        $this->deliveryWindowEnd = $deliveryWindowEnd;
    }

    public function customerId(): string
    {
        return $this->clientId;
    }

    public function deliveryDate(): \DateTime
    {
        return $this->deliveryDate;
    }

    public function deliveryWindowStart(): int
    {
        return $this->deliveryWindowStart;
    }

    public function deliveryWindowEnd(): int
    {
        return $this->deliveryWindowEnd;
    }
}
