<?php

namespace App\Application\Order\CreateOrder;

use App\Application\Order\AssignDriver\AssignDriverCommand;
use App\Domain\Order\OrderCreated;
use App\Domain\Order\OrderRepository;
use Illuminate\Contracts\Bus\Dispatcher;

final class OrderCreatedSubscriber
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    public function __construct(OrderRepository $orderRepository, Dispatcher $dispatcher)
    {
        $this->orderRepository = $orderRepository;
        $this->dispatcher = $dispatcher;
    }

    public function handle(OrderCreated $event): void
    {
        $orderId = $event->orderId();

        $order = $this->orderRepository->findOrFail($orderId);

        $command = new AssignDriverCommand($order->id());

        $this->dispatcher->dispatch($command);
    }
}
