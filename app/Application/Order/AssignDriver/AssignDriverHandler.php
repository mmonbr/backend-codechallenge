<?php

namespace App\Application\Order\AssignDriver;

use App\Domain\Order\OrderRepository;

final class AssignDriverHandler
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function handle(AssignDriverCommand $command)
    {
        $orderId = $command->orderId();

        $order = $this->orderRepository->findOrFail($orderId);

        $order->setDriver('RANDOM_DRIVER_ID');

        $this->orderRepository->save($order);
    }
}
