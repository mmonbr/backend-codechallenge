<?php

namespace App\Application\Order\AssignDriver;

final class AssignDriverCommand
{
    /**
     * @var string
     */
    private $orderId;

    public function __construct(string $orderId)
    {
        $this->orderId = $orderId;
    }

    public function orderId(): string
    {
        return $this->orderId;
    }
}
