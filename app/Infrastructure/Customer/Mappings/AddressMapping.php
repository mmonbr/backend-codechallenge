<?php

namespace App\Infrastructure\Customer\Mappings;

use App\Domain\Customer\Address;
use App\Domain\Customer\Customer;
use LaravelDoctrine\Fluent\Builders\GeneratedValue;
use LaravelDoctrine\Fluent\EntityMapping;
use LaravelDoctrine\Fluent\Fluent;

final class AddressMapping extends EntityMapping
{
    public function mapFor(): string
    {
        return Address::class;
    }

    public function map(Fluent $builder): void
    {
        $builder->guid('id')->generatedValue(function (GeneratedValue $value) {
            $value->uuid();
        })->primary();

        $builder->string('street');
        $builder->string('city');
        $builder->string('state');
        $builder->string('country');
        $builder->string('zipCode');

        $builder->manyToOne(Customer::class)->inversedBy('addresses');
    }
}
