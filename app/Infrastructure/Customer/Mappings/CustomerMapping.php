<?php

namespace App\Infrastructure\Customer\Mappings;

use App\Domain\Customer\Address;
use App\Domain\Customer\Customer;
use LaravelDoctrine\Fluent\Builders\GeneratedValue;
use LaravelDoctrine\Fluent\EntityMapping;
use LaravelDoctrine\Fluent\Fluent;

final class CustomerMapping extends EntityMapping
{
    public function mapFor(): string
    {
        return Customer::class;
    }

    public function map(Fluent $builder): void
    {
        $builder->guid('id')->generatedValue(function (GeneratedValue $value) {
            $value->uuid();
        })->primary();

        $builder->string('firstName');
        $builder->string('lastName');
        $builder->string('email')->unique();
        $builder->string('phoneNumber');

        $builder->oneToMany(Address::class)->mappedBy('customer')->fetchEager();
    }
}
