<?php


namespace App\Infrastructure\Customer\Providers;


use App\Domain\Customer\Customer;
use App\Domain\Customer\CustomerRepository;
use App\Infrastructure\Customer\Repositories\DoctrineCustomerRepository;
use Illuminate\Support\ServiceProvider;
use LaravelDoctrine\ORM\Facades\EntityManager;

final class CustomerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CustomerRepository::class, function(){
            return new DoctrineCustomerRepository(
                EntityManager::getRepository(Customer::class)
            );
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
