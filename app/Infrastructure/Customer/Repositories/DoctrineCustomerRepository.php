<?php

namespace App\Infrastructure\Customer\Repositories;

use App\Domain\Customer\Customer;
use App\Domain\Customer\CustomerNotFoundException;
use App\Domain\Customer\CustomerRepository;
use Doctrine\Common\Persistence\ObjectRepository;

final class DoctrineCustomerRepository implements CustomerRepository
{
    /**
     * @var ObjectRepository
     */
    private $genericRepository;

    public function __construct(ObjectRepository $genericRepository)
    {
        $this->genericRepository = $genericRepository;
    }

    public function findOrFail(string $id): Customer
    {
        $customer = $this->genericRepository->find($id);

        if (null === $customer) {
            throw new CustomerNotFoundException();
        }

        return $customer;
    }
}
