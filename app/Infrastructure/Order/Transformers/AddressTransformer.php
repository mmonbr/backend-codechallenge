<?php

namespace App\Infrastructure\Order\Transformers;

use App\Domain\Order\Address;
use League\Fractal\TransformerAbstract;

final class AddressTransformer extends TransformerAbstract
{
    public function transform(Address $address): array
    {
        return [
            'street'  => $address->street(),
            'city'    => $address->city(),
            'state'   => $address->state(),
            'country' => $address->country(),
            'zipCode' => $address->zipCode(),
        ];
    }
}
