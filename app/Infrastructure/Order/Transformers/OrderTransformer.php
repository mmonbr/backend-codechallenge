<?php

namespace App\Infrastructure\Order\Transformers;

use App\Domain\Order\Order;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

final class OrderTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'address',
        'deliveryWindow',
    ];

    public function transform(Order $order): array
    {
        return [
            'id'          => $order->id(),
            'customer'    => [
                'firstName'   => $order->firstName(),
                'lastName'    => $order->firstName(),
                'phoneNumber' => $order->firstName(),
                'email'       => $order->firstName(),
            ],
            'driver'      => $order->driver(),
            'deliveredAt' => $order->deliveredAt()
        ];
    }

    public function includeAddress(Order $order): Item
    {
        $address = $order->address();

        return $this->item($address, new AddressTransformer);
    }

    public function includeDeliveryWindow(Order $order): Item
    {
        $deliveryWindow = $order->deliveryWindow();

        return $this->item($deliveryWindow, new DeliveryWindowTransformer);
    }
}
