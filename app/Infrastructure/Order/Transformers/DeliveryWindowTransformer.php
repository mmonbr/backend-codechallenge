<?php

namespace App\Infrastructure\Order\Transformers;

use App\Domain\Order\DeliveryWindow;
use League\Fractal\TransformerAbstract;

final class DeliveryWindowTransformer extends TransformerAbstract
{
    public function transform(DeliveryWindow $deliveryWindow): array
    {
        return [
            'deliveryWindowStart' => $deliveryWindow->start(),
            'deliveryWindowEnd'   => $deliveryWindow->end(),
        ];
    }
}
