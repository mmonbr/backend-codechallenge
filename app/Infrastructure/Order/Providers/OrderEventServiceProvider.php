<?php

namespace App\Infrastructure\Order\Providers;

use App\Application\Order\CreateOrder\OrderCreatedSubscriber;
use App\Domain\Order\OrderCreated;
use App\Infrastructure\Laravel\Providers\EventServiceProvider;

final class OrderEventServiceProvider extends EventServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        OrderCreated::class => [
            OrderCreatedSubscriber::class,
        ],
    ];
}
