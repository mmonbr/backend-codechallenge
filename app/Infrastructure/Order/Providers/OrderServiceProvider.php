<?php

namespace App\Infrastructure\Order\Providers;

use App\Application\Order\AssignDriver\AssignDriverCommand;
use App\Application\Order\AssignDriver\AssignDriverHandler;
use App\Application\Order\CreateOrder\CreateOrderCommand;
use App\Application\Order\CreateOrder\CreateOrderHandler;
use App\Domain\Order\Order;
use App\Domain\Order\OrderRepository;
use App\Infrastructure\Order\Repositories\DoctrineOrderRepository;
use Illuminate\Support\ServiceProvider;

final class OrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app('Illuminate\Bus\Dispatcher')->map(
            [CreateOrderCommand::class => CreateOrderHandler::class]
        );

        app('Illuminate\Bus\Dispatcher')->map(
            [AssignDriverCommand::class => AssignDriverHandler::class]
        );

        $this->app->bind(OrderRepository::class, function ($app) {
            return new DoctrineOrderRepository(
                $app['em'],
                $app['em']->getClassMetaData(Order::class)
            );
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

