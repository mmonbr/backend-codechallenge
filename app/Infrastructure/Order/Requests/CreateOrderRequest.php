<?php

namespace App\Infrastructure\Order\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateOrderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'customerId'          => 'required',
            'deliveryDate'        => 'required|date',
            'deliveryWindowStart' => 'required|numeric|between:0,24',
            'deliveryWindowEnd'   => 'required|numeric|between:0,24',
        ];
    }

    protected function failedValidation(Validator $validator): void
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
