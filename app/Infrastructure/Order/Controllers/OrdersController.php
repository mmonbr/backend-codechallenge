<?php

namespace App\Infrastructure\Order\Controllers;

use App\Application\Order\CreateOrder\CreateOrderCommand;
use App\Domain\Order\OrderFilter;
use App\Domain\Order\OrderRepository;
use App\Infrastructure\Laravel\Http\Controllers\Controller;
use App\Infrastructure\Order\Requests\CreateOrderRequest;
use App\Infrastructure\Order\Transformers\OrderTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

final class OrdersController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function index(Request $request)
    {
        $deliveryDate = new \DateTime($request->get('deliveryDate'));
        $driverId = $request->get('driverId');

        $orderFilter = new OrderFilter();
        $orderFilter->setDeliveryDate($deliveryDate);
        $orderFilter->setDriverId($driverId);

        $orders = $this->orderRepository->findAll($orderFilter);

        $fractal = new Manager();
        $resource = new Collection($orders, new OrderTransformer());
        $transformed = $fractal->createData($resource)->toArray();

        return new Response($transformed, Response::HTTP_OK);
    }

    public function store(CreateOrderRequest $request): Response
    {
        $customerId = $request->get('customerId');
        $deliveryDate = new \DateTime($request->get('deliveryDate'));
        $deliveryWindowStart = $request->get('deliveryWindowStart');
        $deliveryWindowEnd = $request->get('deliveryWindowEnd');

        $command = new CreateOrderCommand(
            $customerId,
            $deliveryDate,
            $deliveryWindowStart,
            $deliveryWindowEnd
        );

        $this->dispatch($command);

        return new Response('', Response::HTTP_CREATED);
    }
}
