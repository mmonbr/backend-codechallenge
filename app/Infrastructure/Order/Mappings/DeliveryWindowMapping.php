<?php

namespace App\Infrastructure\Order\Mappings;

use App\Domain\Order\DeliveryWindow;
use LaravelDoctrine\Fluent\EmbeddableMapping;
use LaravelDoctrine\Fluent\Fluent;

final class DeliveryWindowMapping extends EmbeddableMapping
{
    public function mapFor(): string
    {
        return DeliveryWindow::class;
    }

    public function map(Fluent $builder): void
    {
        $builder->integer('start');
        $builder->integer('end');
    }
}
