<?php

namespace App\Infrastructure\Order\Mappings;

use App\Domain\Order\Address;
use App\Domain\Order\DeliveryWindow;
use App\Domain\Order\Order;
use LaravelDoctrine\Fluent\Builders\GeneratedValue;
use LaravelDoctrine\Fluent\EntityMapping;
use LaravelDoctrine\Fluent\Fluent;

final class OrderMapping extends EntityMapping
{
    public function mapFor(): string
    {
        return Order::class;
    }

    public function map(Fluent $builder): void
    {
        $builder->guid('id')->generatedValue(function (GeneratedValue $value) {
            $value->uuid();
        })->primary();

        $builder->string('firstName');
        $builder->string('lastName');
        $builder->string('email');
        $builder->string('phoneNumber');
        $builder->string('driver')->nullable();

        $builder->dateTime('deliveryDate');
        $builder->dateTime('deliveredAt')->nullable();

        $builder->embed(Address::class);
        $builder->embed(DeliveryWindow::class);
    }
}
