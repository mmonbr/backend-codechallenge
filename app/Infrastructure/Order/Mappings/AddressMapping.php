<?php

namespace App\Infrastructure\Order\Mappings;

use App\Domain\Order\Address;
use LaravelDoctrine\Fluent\EmbeddableMapping;
use LaravelDoctrine\Fluent\Fluent;

final class AddressMapping extends EmbeddableMapping
{
    public function mapFor(): string
    {
        return Address::class;
    }

    public function map(Fluent $builder): void
    {
        $builder->string('street');
        $builder->string('city');
        $builder->string('state');
        $builder->string('country');
        $builder->string('zipCode');
    }
}
