<?php

namespace App\Infrastructure\Order\Repositories;

use App\Domain\Order\Order;
use App\Domain\Order\OrderFilter;
use App\Domain\Order\OrderNotFoundException;
use App\Domain\Order\OrderRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

final class DoctrineOrderRepository extends EntityRepository implements OrderRepository
{
    public function findOrFail(string $id): Order
    {
        $order = $this->find($id);

        if (null === $order) {
            throw new OrderNotFoundException();
        }

        return $order;
    }

    /**
     * @return Order[]
     */
    public function findAll(?OrderFilter $filter = null): array
    {
        $builder = $this->createQueryBuilder('orders');

        $this->applyFilter($filter, $builder);

        return $builder->getQuery()->getResult();
    }

    public function save(Order $order): void
    {
        $this->getEntityManager()->persist($order);
        $this->getEntityManager()->flush();
    }

    private function applyFilter(OrderFilter $filter, QueryBuilder $builder)
    {
        if (null === $filter) {
            return;
        }

        $driverId = $filter->driverId();

        if (null !== $driverId) {
            $builder->andWhere('orders.driver = :driver');
            $builder->setParameter('driver', $driverId);
        }

        $deliveryDate = $filter->deliveryDate();

        if (null !== $deliveryDate) {
            $builder->andWhere('orders.deliveryDate = :deliveryDate');
            $builder->setParameter('deliveryDate', $deliveryDate);
        }
    }
}
